class Node:
    # Constructor ile yeni node yapiyoruz. Node'un
    # left ve right olmak uzere iki child`i var.
    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None
def insert(node, key):
    # tree bos ise yeni bir node olustur.
    if node is None:
        return Node(key)

    # doluysa recursive bir sekilde agaci gez
    if key < node.key:
        node.left = insert(node.left, key)
    elif key > node.key:
        node.right = insert(node.right, key)

    # node pointerini returnle
    return node
def search(root, key):
    # Base Case: root yoksa veya key root key`e esit ise
    if root is None or root.key == key:
        return root

    # Key root.key`den buyuk ise
    if root.key < key:
        return search(root.right, key)

    # Key root.key`den kucuk ise
    return search(root.left, key)
def printInorder(root):
    if root:
        printInorder(root.left)
        print(root.key, end=" ")
        printInorder(root.right)
def printPreorder(root):
    if root:
        print(root.key, end=" ")
        printPreorder(root.left)
        printPreorder(root.right)
def printPostorder(root):
    if root:
        printPostorder(root.left)
        printPostorder(root.right)
        print(root.key, end=" ")
def printLeaves(root):
    #farkli bir arama sekli oldugu icin merak edip bunu da
    #implemente ettim. text dosyasi icin daha cok ornek olsun istedim
    #bu traversal cesidi ise yapraklardan basliyor ve saat yonu tersine dogru
    #yapraklari print ediyor.
    if (root):
        printLeaves(root.left)


        if root.left is None and root.right is None:
            print(root.key),

        printLeaves(root.right)
def printBoundaryLeft(root):
    if (root):
        if (root.left):

            # yukaridan asagi gitmesi icin
            # nodeu cagirmadan once yazdirdik.(printBoundaryLeft ile cagirilmadan)
            print(root.key)
            printBoundaryLeft(root.left)

        elif (root.right):
            print(root.key)
            printBoundaryLeft(root.right)
def printBoundaryRight(root):

 if (root):
    if (root.right):
        # saat yonunun tersinde gidecegimiz icin ilk
        # sagda kalan subtreeyi sonra bunu yazdirdik.
        printBoundaryRight(root.right)
        print(root.key)

    elif (root.left):
        printBoundaryRight(root.left)
        print(root.key)

    # leaf node ise bir sey yapmiyoruz. boylece ayni node
    #birden fazla kez cikmamis oluyor.
def printBoundary(root):
    if (root):
        print(root.key)
        #soldaki siniri yazdirdik
        printBoundaryLeft(root.left)

        #o leveldaki leafleri yazdirdik
        printLeaves(root.left)
        printLeaves(root.right)

        #ayni sekilde sagdaki sinirlari yukaridan asagi yazdirdik
        printBoundaryRight(root.right)
def diagonalPrintUtil(root, d, diagonalPrintMap):
    # recursive kullanildigi icin bir base case olmali
    if root is None:
        return

    # ayni dogrudaki nodelar icin bir array.
    try:
        diagonalPrintMap[d].append(root.key)
    except KeyError:
        diagonalPrintMap[d] = [root.key]
    diagonalPrintUtil(root.left,d + 1, diagonalPrintMap)
    diagonalPrintUtil(root.right,d, diagonalPrintMap)
    #left child icin d yani level 1 artacak cunku asagi caprazdan devam edecegiz
    #ancak right child hala ayni baglantidan cikacak.
def diagonalPrint(root):
    # capraz nodelari tutmasi icin bir dict() yarattik.
    diagonalPrintMap = dict()

    # yukaridaki fonksiyon ile yolu buluyoruz.
    diagonalPrintUtil(root, 0, diagonalPrintMap)
    #print ediyoruz.
    print("Diagonal Traversal of binary tree : ")
    for i in diagonalPrintMap:
        for j in diagonalPrintMap[i]:
            print(j, end=" ")
        print()







import random


#    for i in range (0,20,1):
#        print(f"insert(root,{random.randint(0,100)}) ")  output kismindan kopyala yapistir insert methodu


import time

if __name__ == '__main__':

    root = None
    root = insert(root, 50)
    insert(root, 30)
    insert(root, 20)
    insert(root, 40)
    insert(root, 70)
    insert(root, 60)
    insert(root, 80)
    insert(root, 150)
    insert(root, 0)
    insert(root, 16)
    insert(root, 90)
    insert(root, 67)
    insert(root, 94)
    insert(root, 66)
    insert(root, 47)
    insert(root, 97)
    insert(root, 82)
    insert(root, 26)
    insert(root, 14)
    insert(root, 48)
    insert(root, 42)
    insert(root, 43)
    insert(root, 88)
    insert(root, 21)
    insert(root, 92)
    insert(root, 85)

    #tree hazir


    start1 = time.time()
    print("")
    printPostorder(root)
    end1 = time.time()
    print(f"Post order traversal took {end1  - start1} seconds.")
    start2 = time.time()
    printPreorder(root)
    end2 = time.time()
    print(f"Pre order traversal took {end2 - start2} seconds.")
    start3 = time.time()
    printInorder(root)
    end3 = time.time()
    print(f"In order traversal took {end3-start3} seconds")
    start4 = time.time()
    diagonalPrint(root)
    end4 = time.time()
    print(f"Diagonal Traversal took {end4 - start4} seconds")
    start5 = time.time()
    printBoundary(root)
    end5 = time.time()
    print(f"Boundary Traversal took {end5 - start5}seconds")

    time1 = end1 - start1
    time2 = end2- start2
    time3 = end3 - start3
    time4 = end4 - start4
    time5 = end5 - start5

    print(f"Time1 is {time1} \n"
          f"Time2 is {time2} \n"
          f"Time3 is {time3} \n"
          f"Time4 is {time4} \n"
          f"Time5 is {time5} \n")

